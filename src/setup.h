//setup.h ADSP-21369 v.1
       
#include 	<sru.h>
#include 	<21369.h>
#include 	<signal.h>
#include 	<sysreg.h>

//dodatkowe funkcje==========================
//#define stanowisko1
//#define stanowisko2
//#define stanowisko3			
//#define kalibracja_potencjometrow
//===========================================

#define	f_TIMER		1000				//czestotliwosc przerwan Timera [Hz]
#define f_OSC	 	(20e6)			//wpisz czest. kwarca [Hz]		
#define DSP_CLK 	(350e6)			//wpisz czest. DSP

unsigned int 	TMR_RATE	=	DSP_CLK/f_TIMER;
unsigned char 	znak[8];

void InitSRU()
{
	// Enable pull-up resistors on unused DAI pins
	* (volatile int *)DAI_PIN_PULLUP = 0xFFFF7;

	// Enable pull-up resistors on unused DPI pins
	* (volatile int *)DPI_PIN_PULLUP = 0x1FFC;

	//Generating Code for connecting : PCG_FSA to DAI_PIN4
	SRU (HIGH, PBEN04_I); 
	SRU (PCG_FSA_O, DAI_PB04_I); 

	//Generating Code for connecting : LOW to DPI_PIN14
	SRU (HIGH, DPI_PBEN14_I); 
	SRU (LOW, DPI_PB14_I); 

	//Generating Code for connecting : HIGH to DPI_PIN1
	SRU (HIGH, DPI_PBEN01_I); 
	SRU (HIGH, DPI_PB01_I); 

	//Generating Code for connecting : TIMER0 to DPI_PIN2
	SRU (HIGH, DPI_PBEN02_I); 
	SRU (TIMER0_O, DPI_PB02_I); 
}
//===========================================================


void PLL_setup()// PLL SETUP ------->   350MHz
{
	volatile int pmctlsetting;
    pmctlsetting= *pPMCTL;
    pmctlsetting &= ~(0xFF); 		//Clear

    pmctlsetting= SDCKR2|PLLM35|INDIV|DIVEN;
    *pPMCTL= pmctlsetting;
    pmctlsetting|= PLLBP;
    *pPMCTL= pmctlsetting;

    //Wait for around 4096 cycles for the pll to lock.
    for (int i=0; i<4096; i++) asm("nop;nop;");
          
    *pPMCTL ^= PLLBP;       //Clear Bypass Mode
    *pPMCTL |= (CLKOUTEN);  //and start clkout	
}


//  --== FPGA ==-- --== FPGA ==-- --== FPGA ==-- --== FPGA ==-- --== FPGA ==--
void setupFPGA()
{
	set_flag(SET_FLAG3, SET_FLAG);
	*(p_CS_FPGA+0x0E)=13; 		//ustawienia rozdzielczosci enkodera 0x0B-12bit.
	set_flag(SET_FLAG3, CLR_FLAG);
}
//  --== FPGA ==-- --== FPGA ==-- --== FPGA ==-- --== FPGA ==-- --== FPGA ==--

static void InitDPI()
{
	SRU2(UART0_TX_O,DPI_PB09_I); // UART transmit signal is connected to DPI pin 9
	SRU2(HIGH,DPI_PBEN09_I);
	SRU2(DPI_PB10_O,UART0_RX_I); // connect the pin buffer output signal to the UART0 receive
	SRU2(LOW,DPI_PB10_I);
	SRU2(LOW,DPI_PBEN10_I);      // disables DPI pin10 as input
}
//============================================================================
static void UARTisr(int)
{
 	if (*pUART0IIR & UARTRBFI)
 	{
			//	if ( buf.in<BUF_SIZE) buf.buf_rx [buf.in++] = (*pUART0RBR & 0x1FF);
			//	timeout=0; 
			znak[0]=(char) (*pUART0RBR & 0x1FF);    
 	}
}
//============================================================================


void UART_Send(unsigned char *xmit, int SIZE)
{
	int i;
	/* loop to transmit source array in core driven mode */   
  	for (i=0; i<SIZE; i++)
  	{
    	do { ;}// Wait for the UART transmitter to be ready
    	while ((*pUART0LSR & UARTTHRE) == 0);
       	*pUART0THR = xmit[i]; //Transmit a byte
   	}
  
/* poll to ensure UART has completed the transfer */
  	while ((*pUART0LSR & UARTTEMT) == 0)  {;} 
}
//============================================================================

static void InitUART()
{
	*pUART0LCR = UARTDLAB;  
	*pUART0DLL = 0x39; 
	*pUART0DLH = 0x02;  
	*pUART0LCR = UARTWLS8;
	*pUART0RXCTL = UARTEN;       
	*pUART0TXCTL = UARTEN;
    
	*pPICR0 &= ~(0x1F); //Sets the UART0 receive interrupt to P0
	*pPICR0 |= (0x13); 
	*pUART0RXCTL = UARTEN;       //enables UART0 in receive mode
	*pUART0TXCTL = UARTEN;       //enables UART0 in core driven mode
	*pUART0IER   = UARTRBFIE;    //enables UART0 receive interrupt
	interrupt(SIG_P0,UARTisr); 
}

//======================================================================
//--------------------------- setup()  BEGIN----------------------------
void setup()
{
	set_flag(SET_FLAG0, CLR_FLAG);
 	set_flag(SET_FLAG1, CLR_FLAG); 
 	set_flag(SET_FLAG3, CLR_FLAG);
 		
	PLL_setup();
	
	InitDPI();
	InitUART();
	InitSRU();
	
	interrupt(SIG_TMZ,timer_isr); //przerwanie od timera (LOW PRIOYTET.)
	timer_set(TMR_RATE , 0);//   (DSP_CLK/Hz)
	
/* timer_set - description: 
The timer_set function sets the ADSP-21xxx timer registers TPERIOD and TCOUNT.
The function returns a 1 if the timer is enabled, or a zero if the timer is disabled.
*/

    
	 *pAMICTL0=0;
	 *pAMICTL0=(AMIEN | BW32 | WS16 | HC2 | RHC2| IC2);// 
	  
		
	sysreg_write(sysreg_USTAT4,*pSYSCTL);
	sysreg_bit_set(sysreg_USTAT4,IRQ2EN);	//FLAG2 configure as IRQ2
	*pSYSCTL=sysreg_read(sysreg_USTAT4);
		
   	*pSYSCTL	=*pSYSCTL&(~MSEN)&(~TMREXPEN);	//FLAG3 jako wyjscie (zeruj bit MSEN)

	 setupFPGA();
	 LCD_INIT();
	 
	 timer_on(); 

}
//--------------------------- setup() END -------------------------rev.01.03.2020
//===============================================================================

